const forms = document.getElementById('form');
const inputName = document.getElementById('inputName');
const emailAddress = document.getElementById('emailAddress');
const contactFormMessage = document.getElementById('contactFormMessage');

forms.addEventListener('submit', (event) => {
  event.preventDefault();
  checkInputs();
  submitForm();
});

function checkInputs() {
  const inputNameValue = inputName.value.trim();
  const emailAddressValue = emailAddress.value.trim();
  const contactFormMessageValue = contactFormMessage.value.trim();
  if (inputNameValue === '' || inputNameValue === null) {
    setErrorFor(inputName, 'First name cannot be blank');
  } else {
    setSuccessFor(inputName);
  }

  if (emailAddressValue === '' || emailAddressValue === null) {
    setErrorFor(emailAddress, 'Email cannot be blank');
  } else if (!isEmail(emailAddressValue)) {
    setErrorFor(emailAddress, 'Email is not valid');
  } else {
    setSuccessFor(emailAddress);
  }

  if (contactFormMessageValue === '' || contactFormMessageValue === null) {
    setErrorFor(contactFormMessage, 'Please type your query ');
  } else {
    setSuccessFor(contactFormMessage);
  }
}

function setErrorFor(input, message) {
  const formGroup = input.parentElement;
  const small = formGroup.querySelector('small');
  small.innerText = message;
  formGroup.className = 'was-validated ';
}

function setSuccessFor(input, message) {
  const formGroup = input.parentElement;
  formGroup.className = 'was-validated';
}

function isEmail(email) {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

function submitForm() {
  const scriptURL =
    'https://script.google.com/macros/s/AKfycbwfSDAEouNYCyWQTYHTFPDcHB2t0vGBVyQDCugjqu9MrK8U9SI/exec';
  const form = document.forms['submit-to-google-sheet'];

  form.addEventListener('submit', (event) => {
    event.preventDefault();
    fetch(scriptURL, { method: 'POST', body: new FormData(form) })
      .then((response) => console.log('Success!', response))
      .catch((error) => console.log('Error', error.message));
  });
}
